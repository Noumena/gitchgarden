﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Shredders : MonoBehaviour {
	
	private void OnTriggerExit2D (Collider2D other)
	{
		GameObject obj = other.gameObject;
		if (obj.GetComponent<Attackers> ()) 
		{
			WinLoseManager.Instance.LoseCondition ();

		} else {
			Destroy (other.gameObject);
		}
	}


}
