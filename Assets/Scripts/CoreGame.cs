﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridCell
{
    public Defenders defender;
}

public class CoreGame : MonoBehaviour {

    private List<List<GridCell>> gridCells;
    private int gridIndexX, gridIndexY;
    void Start ()
    {
        gridCells = new List<List<GridCell>>()
        {
            new List<GridCell>{new GridCell(), new GridCell(), new GridCell(), new GridCell(), new GridCell(), },
            new List<GridCell>{new GridCell(), new GridCell(), new GridCell(), new GridCell(), new GridCell(), },
            new List<GridCell>{new GridCell(), new GridCell(), new GridCell(), new GridCell(), new GridCell(), },
            new List<GridCell>{new GridCell(), new GridCell(), new GridCell(), new GridCell(), new GridCell(), },
            new List<GridCell>{new GridCell(), new GridCell(), new GridCell(), new GridCell(), new GridCell(), },
            new List<GridCell>{new GridCell(), new GridCell(), new GridCell(), new GridCell(), new GridCell(), },
            new List<GridCell>{new GridCell(), new GridCell(), new GridCell(), new GridCell(), new GridCell(), },
            new List<GridCell>{new GridCell(), new GridCell(), new GridCell(), new GridCell(), new GridCell(), },
            new List<GridCell>{new GridCell(), new GridCell(), new GridCell(), new GridCell(), new GridCell(), },
        };
    }
	
    public Defenders Gridcells(int mousePos2DIntx, int mousePos2DInty)
    {
        SetGridIndex(mousePos2DIntx, mousePos2DInty);
        return gridCells[gridIndexX][gridIndexY].defender;
    }

    public void SpawnInGridCells(int mousePos2DIntx, int mousePos2DInty,  Defenders defender)
    {
        SetGridIndex(mousePos2DIntx, mousePos2DInty);       
        gridCells[gridIndexX][gridIndexY].defender = Instantiate(defender, new Vector2(mousePos2DIntx, mousePos2DInty), Quaternion.identity); 
    }

    private void SetGridIndex(int mousePos2Dintx, int mousePos2DIntY)
    {
        gridIndexX = mousePos2Dintx - 1;
        gridIndexY = mousePos2DIntY - 1;
    }
   
}
