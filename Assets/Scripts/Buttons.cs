﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Buttons : MonoBehaviour {


	[SerializeField] private Transform selectedPrefabs;
    public Transform SelectedPrefabs
    {
        get { return selectedPrefabs; }
    }
	private Text costText;

	private void Start () 
	{
        Defenders defenders = selectedPrefabs.GetComponent<Defenders>();
		if (transform.childCount > 0 && defenders != null)

        {
            SetCostTextForDefenders(defenders);
        }

        else 
		{
			return;
		}
	}

    private void SetCostTextForDefenders(Defenders defenders)
    {
        int CostValue = defenders.StarValue;
        costText = GetComponentInChildren<Text>();
        costText.text = CostValue.ToString();
    }
}
