﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour {

    private AudioSource audioSource;
    [SerializeField] private AudioClip[] MusicLevelArray;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        audioSource = GetComponent<AudioSource>();  
    }
  
    private void OnEnable()
    { 
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    private void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        int level = scene.buildIndex;
        AudioClip thisLevelMusic = MusicLevelArray[level];
        if (thisLevelMusic)
        {
            if (audioSource)
            {
                audioSource.clip = thisLevelMusic;
                audioSource.loop = true;
                audioSource.Play();
                audioSource.volume = PlayerPrebsManager.GetMasterVolumeKey();
            }
        }
    }

	public void ChangeVolume (float changeVolume)
	{
		audioSource.volume = changeVolume;
	}
}
