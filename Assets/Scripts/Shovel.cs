﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shovel : MonoBehaviour {

	
	[SerializeField] private float fullSellTime;
    public float FullTimeSell
    {
        get { return fullSellTime; }
    }

    public void selldefenders(Defenders defenders)
    {
        int sellingValue;
        if (defenders.TimeOfTheDefenders () <= fullSellTime)
        {
            Debug.Log(defenders.StarValue);
            sellingValue = defenders.StarValue;
            WinLoseManager.Instance.GetComponent<StarDisplay>().AddStars(sellingValue);
            Destroy(defenders.gameObject);
        }
        else
        {
            sellingValue = (defenders.StarValue) / 2;
            WinLoseManager.Instance.GetComponent<StarDisplay>().AddStars(sellingValue);
            Destroy(defenders.gameObject);
        }     
    }
}