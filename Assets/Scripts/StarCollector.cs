﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarCollector : Defenders
{
    private StarDisplay starDisplay;

    private void Start ()
    {
        starDisplay = WinLoseManager.Instance.GetComponent<StarDisplay>();
    }

    private void PlusStars(int stars)
    {
        starDisplay.AddStars(stars);
    }

}
