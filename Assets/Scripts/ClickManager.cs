﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class ClickManager : MonoBehaviour {

	private Buttons[] ButtonsArray;
	private Transform SelectedButtons;
	private GameObject defendersParent;
	

    private void Start ()
    {     
        ButtonsArray = GameObject.FindObjectsOfType<Buttons>();
        SetDefenderParent();
    }

   

    private void SetDefenderParent()
    {
        defendersParent = GameObject.Find("Defenders");
        if (!defendersParent)
        {
            defendersParent = new GameObject();
            defendersParent.name = "Defenders";
        }
    }

    private void Update()
    {
        if (Time.timeScale == 1)
        {
            ManageClick();
        }
    }

    private void ManageClick ()
	{
		if (Input.GetMouseButtonDown (0))
        {           
            OnRayCast();
        }
    }

    private void OnRayCast()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 mousepos2D = new Vector2(mousePos.x, mousePos.y);
        int mousePos2DIntX = Mathf.RoundToInt(mousepos2D.x);
        int mousePos2DIntY = Mathf.RoundToInt(mousepos2D.y);
        Vector2 mousePos2DInt = new Vector2(mousePos2DIntX, mousePos2DIntY);
        RaycastHit2D hit = Physics2D.Raycast(mousePos2DInt, Vector2.zero, 1<<8); 

            if (hit.collider != null)
            {
                if (hit.collider.GetComponent<Buttons>())
                {
                    Selected(hit);
                }

                else if (hit.collider.GetComponent<CoreGame>())
                {
                    CoreGame coreGame = hit.collider.GetComponent<CoreGame>();
               
                        if (SelectedButtons.GetComponent<Defenders>())
                        {
                            if (!coreGame.Gridcells(mousePos2DIntX, mousePos2DIntY))
                            {
                                StarDisplay starDisplay = GetComponent<StarDisplay>();
                                Defenders defenderPrefab = SelectedButtons.GetComponent<Defenders>();
                                if (starDisplay.MinusStars(defenderPrefab.StarValue) == StarDisplay.Status.TRUE)
                                {
                                    coreGame.SpawnInGridCells(mousePos2DIntX, mousePos2DIntY, defenderPrefab);
                                }
                            }
                            else
                            {
                                Debug.Log("Cant spawn");
                            }

                        }
                    
                        if (SelectedButtons.GetComponent<Shovel>())
                        {
                            Shovel shovel = SelectedButtons.GetComponent<Shovel>();
                            Debug.Log(mousePos2DIntX + mousePos2DIntY);
                            if (coreGame.Gridcells(mousePos2DIntX, mousePos2DIntY))
                            {
                                Defenders soldDefenders = coreGame.Gridcells(mousePos2DIntX, mousePos2DIntY);
                                shovel.selldefenders(soldDefenders);                                
                            }
                        }
                }

                else if (hit.collider.name == "Pause")
                {
                    WinLoseManager.Instance.PauseControl();
                }

                else if (hit.collider.GetComponent<Attackers>())
                {
                    return;
                }
            }

        else
        {
            Deselected();
        }
        
    }

    private void Deselected()
    {
        foreach (Buttons ThisButtons1 in ButtonsArray)
        {
            ThisButtons1.GetComponent<SpriteRenderer>().color = Color.black;
            SelectedButtons = null;
            Debug.Log(SelectedButtons);
        }
    }

    private void Selected(RaycastHit2D hit)
    {
        foreach (Buttons ThisButtons in ButtonsArray)
        {
            ThisButtons.GetComponent<SpriteRenderer>().color = Color.black;
        }
        hit.collider.GetComponent<SpriteRenderer>().color = Color.white;
        SelectedButtons = hit.collider.GetComponent<Buttons>().SelectedPrefabs;
    }

    
}
