﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour {

	private Slider gameTimer;
	private float GameTime;
    [SerializeField] float[] gameTimeDifficulty;
    private float difficultyLevel;
	bool IsGameEnd = false;
	
    private void Awake()
    {
        gameTimer = GetComponent<Slider>();
        difficultyLevel = PlayerPrebsManager.GetDifficultyKey();
    }
    private void Start ()
    {
        SetDifficultyGameTime();
        Debug.Log(GameTime);
            
    }

    private void SetDifficultyGameTime()
    {
        if (difficultyLevel == 1)
        {
            GameTime = gameTimeDifficulty[0];
        }
        else if (difficultyLevel == 2)
        {
            GameTime = gameTimeDifficulty[1];
        }
        else if (difficultyLevel == 3)
        {
            GameTime = gameTimeDifficulty[2];
        }
    }

    private void Update () 
	{
		gameTimer.value = Time.timeSinceLevelLoad / GameTime;
		if (Time.timeSinceLevelLoad >= GameTime && !IsGameEnd) 
		{
			IsGameEnd = true;
			WinLoseManager.Instance.WinCondition ();
		}
	}

}
