﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectiles : MonoBehaviour {

	private float CurrentSpeed;
	[SerializeField] private int projectilesDamage;

	private void Update () 
	{
		transform.position += new Vector3 (CurrentSpeed * Time.deltaTime, 0f, 0f);	
	}

	private void OnTriggerEnter2D (Collider2D other)
	{
		Attackers attackers = other.gameObject.GetComponent<Attackers>();
       if(attackers != null)
        {
            Health health = attackers.GetComponent<Health>();
            if (health != null)
            {
                health.ReduceHealth(projectilesDamage);
                Destroy(gameObject);
            }               
        }
	}

	private void SetProjectilesSpeed (float speed) // this method get called in animation event.
	{
		CurrentSpeed = speed;
	}
}
