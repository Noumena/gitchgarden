﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

	[SerializeField]private float AutoLoadNextLevelAfter;
	private void Start () 
	{
		if (AutoLoadNextLevelAfter == 0) {
			Debug.Log ("Autoload next level disable");
		} 
		else 
		{
			Invoke ("LoadNextLevel", AutoLoadNextLevelAfter);
		}
	}

	public void LoadStartMenu()
	{
		SceneManager.LoadScene ("01a Start");
	}

	public void LoadOption()
	{
		SceneManager.LoadScene ("01b Option");
	}

	public void LoadNextLevel ()
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);
	}

	public void LoadWin()
	{
		SceneManager.LoadScene ("03a Win");
	}

	public void RestartLevel()
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}

	public void QuitLevel ()
	{
		Application.Quit ();
	}

}
