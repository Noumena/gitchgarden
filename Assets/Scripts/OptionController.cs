﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionController : MonoBehaviour {

	public Slider VolumeSlider;
	public Slider DifficultySlider;
	private MusicManager musicManager;

	private void Start () 
	{
		musicManager = GameObject.FindObjectOfType<MusicManager> ();
		VolumeSlider.value = PlayerPrebsManager.GetMasterVolumeKey ();
		DifficultySlider.value = PlayerPrebsManager.GetDifficultyKey ();

		Debug.Log (PlayerPrebsManager.GetMasterVolumeKey ());
		
	}
	
	private void Update () 
	{
		musicManager.ChangeVolume (VolumeSlider.value);
	}

	public void SaveOption () 
	{
		PlayerPrebsManager.SetMasterVolumeKey (VolumeSlider.value);
		PlayerPrebsManager.SetDifficultyKey (DifficultySlider.value);
		print (VolumeSlider.value);
	}
	public void SetDefault ()
	{
		VolumeSlider.value = 0.8f;
		DifficultySlider.value = 2;
	}
}
