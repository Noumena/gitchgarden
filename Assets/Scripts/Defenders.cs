﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Defenders : MonoBehaviour {

	[SerializeField] private int starValue;   
	private float InitializationTime;
	private float TimeSinceInitialization;

    public int StarValue
    {
        get { return starValue; }
    }

    private void Start () 
	{		
		InitializationTime = Time.timeSinceLevelLoad;
	}

	private void Update () 
	{
		TimeSinceInitialization = Time.timeSinceLevelLoad - InitializationTime; 
	}

	public float TimeOfTheDefenders () 
	{
		return TimeSinceInitialization;
	}
}
