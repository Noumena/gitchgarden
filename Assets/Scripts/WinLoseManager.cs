﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinLoseManager : MonoBehaviour {

	private GameObject[] DestroyedEndGameArray;
	private AudioSource audioSource;
	private MusicManager musicManager;
    private static WinLoseManager instance;
    public static WinLoseManager Instance { get { return instance; } }
    [SerializeField] private Transform[] winningMenu, losingMenu, PausingMenu;    

    private void Awake()
    {
        instance = this;
        Time.timeScale = 1;
        audioSource = GetComponent<AudioSource>();
    }
    void Start () 
	{        
             
		musicManager = GameObject.FindObjectOfType<MusicManager> ();
        HideMenu(PausingMenu);
        HideMenu(winningMenu);
        HideMenu(losingMenu);
    }
	
	public void WinCondition () 
	{
		Time.timeScale = 0;
		DestroyGameEnd ();
		ShowMenu (winningMenu);
		IsMusicPlayer ();
        audioSource.volume = PlayerPrebsManager.GetMasterVolumeKey();
		audioSource.Play ();
	}

	public void LoseCondition () 
	{
		Time.timeScale = 0;
		DestroyGameEnd ();
		ShowMenu (losingMenu);
		IsMusicPlayer ();
        audioSource.volume = PlayerPrebsManager.GetMasterVolumeKey();
        audioSource.Play ();
	}

    private void HideMenu(Transform[] menu)
    {
        foreach (var obj in menu)
        {
            obj.gameObject.SetActive(false);
        }
    }

    private void ShowMenu(Transform[] menu)
    {
        foreach (var obj in menu)
        {
            obj.gameObject.SetActive(true);
        }
    }

    public void PauseControl()
    {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
            ShowMenu(PausingMenu);
        }
        else if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
            HideMenu(PausingMenu);
        }
    }

    private void IsMusicPlayer () 
	{
		if (musicManager) {
			musicManager.GetComponent<AudioSource> ().volume = 0f;
		}
	}

	private void DestroyGameEnd () 
	{
		DestroyedEndGameArray = GameObject.FindGameObjectsWithTag ("DestroyedOnWin");   
		foreach(GameObject destryoedObject in DestroyedEndGameArray)
		{
			Destroy (destryoedObject.gameObject);
		}
	}

}
