﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

	[SerializeField] private int lifepoints;

	public void ReduceHealth (int damage)
	{
        lifepoints -= damage;
        if (lifepoints <= 0)
        {
            Destroy(gameObject);
        }
    } 
}
