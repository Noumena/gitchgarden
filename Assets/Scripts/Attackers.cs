﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attackers : MonoBehaviour
{
    protected Animator anim; // may be change to private
    private Defenders CurrentTarget;
    [SerializeField] private float seenEverySecond;
    private float CurrentSpeed;

    public float SeenEverySecond
    {
        get { return seenEverySecond; }
    }

     private void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
        AttackersMove();
        if (!CurrentTarget)
        {
            anim.SetBool("IsAttacking", false);
        }
    }

    private void AttackersMove()
    {
        transform.position -= new Vector3(CurrentSpeed * Time.deltaTime, 0f);
    }

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        Defenders defenders = other.gameObject.GetComponent<Defenders>();
        if (defenders != null)
        {
            anim.SetBool("IsAttacking", true);
            CurrentTarget = defenders;
        }    
    }

    private void SetSpeed(float speed) // This get called in animation event.
    {
        CurrentSpeed = speed;
    }

    private void StrikingDamage(int Damage) // This get called in animation event.
    {
        if (CurrentTarget != null)
        {
            Health health = CurrentTarget.GetComponent<Health>();            
            if (health != null)
            {
                health.ReduceHealth(Damage);
            }
        }
    }
}

