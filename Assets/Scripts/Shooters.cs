﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooters : Defenders {

	[SerializeField] private Transform gun;
	[SerializeField] private Projectiles ProjectilesPrefabs;
	private GameObject projectilesParent;
	private Animator anim;
	private Spawner[] SpawnerArray;
	private Spawner MylaneSpawner;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }
    private void Start ()
    {
        SpawnerArray = GameObject.FindObjectsOfType<Spawner>();
        SetMyLaneSpawner();
        SetProjectilesParent();
    }

    private void SetProjectilesParent()
    {
        projectilesParent = GameObject.Find("Projectiles");

        if (!projectilesParent)
        {
            projectilesParent = new GameObject();
            projectilesParent.name = "Projectiles";
        }
    }

    private void Update ()
	{
		if (IsAttackersOnLine ()) {
			anim.SetBool ("IsAttacking", true);
		} else 
		{
			anim.SetBool ("IsAttacking", false);	
		}
	}

	private void Fire () 
	{
		Projectiles projectiles = Instantiate(ProjectilesPrefabs, gun.transform.position, Quaternion.identity);
		projectiles.transform.parent = projectiles.transform;
	}

	private void SetMyLaneSpawner () 
	{
		foreach (Spawner spawner in SpawnerArray) {
			if (spawner.transform.position.y == this.transform.position.y) 
			{
				MylaneSpawner = spawner;
				return;
			}
		}
	}

	private bool IsAttackersOnLine ()
	{
		if (MylaneSpawner.transform.childCount <= 0) 
		{
			return false;
		} 
			foreach (Transform attackers in MylaneSpawner.transform) 
		{

				if (attackers.transform.position.x >= this.transform.position.x) 
				{
					return true;
				} 
		}
		return false;
	} 
}	

