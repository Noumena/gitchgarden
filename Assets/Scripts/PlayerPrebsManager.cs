﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerPrebsManager : MonoBehaviour {

	const string MASTER_VOLUME_KEY = "master_volume";
	const string DIFFICULTY_KEY = "difficulty_key";
	const string LEVEL_UNLOCK_KEY = "level_unlock_";

	public static void SetMasterVolumeKey (float volume)
	{
		if (volume >= 0 && volume <= 1f) {
			PlayerPrefs.SetFloat (MASTER_VOLUME_KEY, volume);
		} 

		else 
		{
			Debug.LogWarning ("volume is out of range");
		}
	}

	public static float GetMasterVolumeKey ()
	{
		return PlayerPrefs.GetFloat (MASTER_VOLUME_KEY);
	}

	public static void SetDifficultyKey (float level)
	{
		if (level >= 1f && level <= 3f) {
			PlayerPrefs.SetFloat (DIFFICULTY_KEY, level);
		} 

		else 
		{
			Debug.LogWarning ("The difficulty level does not exist");
		}
	}

	public static float GetDifficultyKey ()
	{
		return PlayerPrefs.GetFloat (DIFFICULTY_KEY, 1f);
	}
}
