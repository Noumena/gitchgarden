﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarDisplay : MonoBehaviour {

	public enum Status {TRUE, FALSE};
	private Text starsText;
	[SerializeField] private int CurrrentStars;

	private void Start () 
	{
		starsText = GetComponent<Text> ();
		UpdateStars ();
	}
	
	public void AddStars (int stars)
	{
		CurrrentStars += stars;
		UpdateStars ();
	}

	public Status MinusStars (int stars)
	{
		if (CurrrentStars >= stars) 
		{
			CurrrentStars -= stars;
			UpdateStars ();
			return Status.TRUE;
		} 

		else 
		{
			return Status.FALSE;
		}
	}

	private void UpdateStars () 
	{
		starsText.text = CurrrentStars.ToString ();
	}
}
