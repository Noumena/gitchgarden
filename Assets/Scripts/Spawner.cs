﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

	[SerializeField] private Attackers[] AttackerPrefabsArray;

	private void Update () 
	{
		foreach (Attackers ThisAttacker in AttackerPrefabsArray) 
		{
			if (TimeToSpawn (ThisAttacker)) 
			{
				SpawnAttacker (ThisAttacker);
			}
		}
	}

	private bool TimeToSpawn (Attackers attackers)
	{
		float MeanSpawnDelay = attackers.SeenEverySecond;
		float SpawnPerSeconds = 1 / MeanSpawnDelay;
		float Threshold = SpawnPerSeconds * Time.deltaTime/5;
		if (Random.value <= Threshold)
        {
			return true;
		}
		else
        {
			return false;	
		}
	}

	private void SpawnAttacker (Attackers attackersPrefabs)
	{
		Attackers attackers = Instantiate (attackersPrefabs, this.transform.position, Quaternion.identity);
		attackers.transform.parent = this.transform;
	}
}
