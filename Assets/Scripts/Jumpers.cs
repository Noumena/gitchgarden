﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumpers : Attackers {

    protected override void OnTriggerEnter2D (Collider2D other) 
	{       
        Defenders defenders = other.GetComponent<Defenders>();
		if (defenders != null) 
		{
            Stone stone = defenders.GetComponent<Stone>();
            if(stone)
            {
                anim.SetTrigger("IsJumping");
            }
            else
            {
                base.OnTriggerEnter2D(other);
            }
		}		
	}
}
